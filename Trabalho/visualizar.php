<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="visualizar.css">
  <title>Imagens</title>
</head>
<header>
<p><a target = "_blank" href='index.html'> Início</a></p>
</header>
<body>
  <h1>Minhas Fotos</h1>
  <br><br>
  <div class="row">
  <?php
  
  $fotos = glob("fotos/*.*");

  for ($i = 0; $i < count($fotos); $i++) {
    echo "<div class='coluna'>";
    if (!empty($fotos[$i])) {
      echo '<img src="' . $fotos[$i] . '">';
    }
    echo "</div>";
  }

  ?>
  </div>
</body>

</html>
