<?php
if (!file_exists("fotos")) {
    mkdir("fotos");
}

$dir = "fotos/"; 

$fotos = array(); 

$file = $_FILES["arquivo"];

//Podemos verificar se o arquivo foi enviado
if (!is_uploaded_file($_FILES['arquivo']['tmp_name'])) {
    header("location: index.html");
}

$extensao = pathinfo($_FILES['arquivo']['name'], PATHINFO_EXTENSION);

if ($extensao == "png") {
    move_uploaded_file($_FILES['arquivo']['tmp_name'], "$dir/" . uniqid() . "." . $extensao);
    echo "<script> window.alert('Sucesso.')
    window.location.href='index.html'; </script>";  
} else {
    echo "<script> window.alert('Escolha um arquivo do tipo .PNG, por favor!') 
    window.location.href='index.html'; </script>"; 
}
?>